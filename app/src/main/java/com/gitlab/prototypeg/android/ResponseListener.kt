package com.gitlab.prototypeg.android

import android.content.Context

import androidx.preference.PreferenceManager

import com.gitlab.prototypeg.data.DollNumber
import com.gitlab.prototypeg.data.Language
import com.gitlab.prototypeg.doll.DollInfo
import com.gitlab.prototypeg.network.Handler
import com.gitlab.prototypeg.network.Handler.Synchronized
import com.gitlab.prototypeg.network.request.DataRequest
import com.gitlab.prototypeg.network.request.ParamRequest
import com.gitlab.prototypeg.network.response.*
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.gson.*
import java.io.File
import java.io.FileOutputStream
import java.io.FileWriter

import java.util.ArrayList
import java.util.Random

class ResponseListener(private val context: Context) : Handler<Response> {
    private val preferences = PreferenceManager.getDefaultSharedPreferences(context)

	@Synchronized
	fun onIndexResponse(indexResponse: IndexResponse) {
        try {
            if (preferences.getBoolean(context.getString(R.string.use_random_adjutant), false)) {
                randomizeAdjutant(indexResponse)
            }

            if (preferences.getBoolean(context.getString(R.string.attempt_decensor), false)) {
                decensor(indexResponse)
                preferences.edit().putBoolean(context.getString(R.string.attempt_decensor), false).apply()
            }
		} catch (e: Throwable) {
            e.printStackTrace()
			FirebaseCrashlytics.getInstance().recordException(e);
        }
    }

    private fun randomizeAdjutant(indexResponse: IndexResponse) {
        val collectedDolls = indexResponse.userInfo?.collectedDolls
		if (collectedDolls === null) {
			return
		}
        val blacklist = ArrayList<DollNumber>()
        val blacklistData = preferences.getString(context.getString(R.string.blacklist), "")!!.split("\\r?\\n".toRegex())
        for (doll in collectedDolls) {
            for (id in blacklistData) {
                try {
                    if (doll == java.lang.Short.valueOf(id)) {
                        blacklist.add(doll)
                        break
                    }
                } catch (e: NumberFormatException) {
                }

            }
        }
        collectedDolls.removeAll(blacklist)

        val random = Random()
        val bound = collectedDolls.size
        if (preferences.getBoolean(context.getString(R.string.include_kalina), true)) {
            //bound++;
        }
        val doll = DollInfo[collectedDolls[random.nextInt(bound)]]
        val skins = doll.skins!!.clone() as ArrayList<Short>
        skins.retainAll(indexResponse.skinsWithUser!!.keys)
        indexResponse.userRecord!!.adjutant!!.dollNumber = doll.dollNumber!!
        if (skins.size != 0) {
            indexResponse.userRecord!!.adjutant!!.skinNumber = skins[random.nextInt(skins.size)]
        } else {
            indexResponse.userRecord!!.adjutant!!.skinNumber = 0.toShort()
        }
        if (preferences.getBoolean(context.getString(R.string.use_damaged), false)) {
            indexResponse.userRecord!!.adjutant!!.isDamaged = random.nextBoolean()
        }
        indexResponse.isEdited = true
    }

    private fun decensor(indexResponse: IndexResponse) {
        indexResponse.decensorFormula = "30:30:30:30"
        indexResponse.isEdited = true
    }

	fun log(response: Response) {

		if (!preferences.getBoolean(context.getString(R.string.use_log_response), false)) {
			return
		}

		val dir = File(context.getExternalFilesDir(null)!!.parentFile.absolutePath + "/logs")
		dir.mkdir()
		val pathBase = dir.absolutePath + "/" + response.request?.path?.replace("/", ".")
		val request = response.request
		val gson = GsonBuilder().setPrettyPrinting().create()

		val requestFileOutputStream = FileOutputStream(pathBase + "_request.txt", false)
		requestFileOutputStream.write(response.buffer ?: ByteArray(0))
		requestFileOutputStream.flush()
		requestFileOutputStream.close()

		val responseFileOutputStream = FileOutputStream(pathBase + "_response.txt", false)
		responseFileOutputStream.write(response.buffer ?: ByteArray(0))
		responseFileOutputStream.flush()
		responseFileOutputStream.close()

		if (request is ParamRequest) {
			val requestParamFileWriter = FileWriter(pathBase + "_request_param.txt", false)
			requestParamFileWriter.write(request.params.toString())
			requestParamFileWriter.flush()
			requestParamFileWriter.close()
			if (request is DataRequest) {
				val requestDataFileWriter = FileWriter(pathBase + "_request_data.json", false)
				requestDataFileWriter.write(gson.toJson(request.data))
				requestDataFileWriter.flush()
				requestDataFileWriter.close()
			}
		}

		if (response is JsonResponse<*>) {
			val responseDataFileWriter = FileWriter(pathBase + "_response_data.json", false)
			responseDataFileWriter.write(gson.toJson(response.data))
			responseDataFileWriter.flush()
			responseDataFileWriter.close()
		}
	}

	@Synchronized
	fun inject(jsonResponse: JsonResponse<*>) {
		if (!preferences.getBoolean(context.getString(R.string.inject_response), false)) {
			return
		}
		val dir = File(context.getExternalFilesDir(null)!!.parentFile.absolutePath + "/injects")
		dir.mkdir()
		val path = dir.absolutePath + "/" + jsonResponse.request?.path?.replace("/", ".") + "_response.json"
		val file: File = File(path)
		if ((!file.exists()) || file.isDirectory) {
			return
		}

		val fileReader = file.reader()
		val jsonElement = JsonStreamParser(fileReader).next()
		fileReader.close()

		if (jsonResponse is JsonObjectResponse && jsonElement is JsonObject) {
			for (s in jsonElement.keySet()) {
				merge(jsonResponse.data, s, jsonElement[s])
			}
		}

		if (jsonResponse is JsonArrayResponse && jsonElement is JsonArray) {
			for (e in jsonElement) {
				jsonResponse.data.add(e)
			}
		}
		jsonResponse.isEdited = true
	}

	private fun merge(base: JsonObject, key: String, target: JsonElement) {
		if (target is JsonObject) {
			var b: JsonElement = base[key]
			if (!(b is JsonObject)) {
				base.add(key, JsonObject())
				b = base[key]
			}
			for (s in target.keySet()) {
				merge(b as JsonObject, s, target[s])
			}
			return
		}
		if (target is JsonArray) {
			val b: JsonElement = base[key]
			if (b is JsonArray) {
				for (e in target) {
					b.add(e)
				}
			} else {
				base.add(key, target)
			}
			return
		}
		base.add(key, target)
	}

	@Synchronized
	fun onFriendVisitResponse(friendVisitResponse: FriendVisitResponse) {
		if (!preferences.getBoolean(context.getString(R.string.minimize_friend_dormitory), false)) {
			return
		}
		val adjutants = JsonArray()

		for (adjutant in friendVisitResponse.data[FriendVisitResponse.ADJUTANT_LIST].asJsonArray) {
			if (adjutant.asJsonObject["f_userid"].asInt == (context.applicationContext as PrototypeG).session.index!!.userInfo!!.userId) {
				adjutants.add(adjutant)
				break
			}
		}

		friendVisitResponse.data.add(FriendVisitResponse.ADJUTANT_LIST, adjutants)
		friendVisitResponse.data.add(FriendVisitResponse.FURNITURE_LIST, JsonArray())
		friendVisitResponse.data.add(FriendVisitResponse.SANGVIS_WITH_USER_LIST, JsonArray())
		friendVisitResponse.data.add(FriendVisitResponse.GUN_WITH_USER_LIST, JsonArray())
		friendVisitResponse.isEdited = true
	}
}
