package com.gitlab.prototypeg.android;

import android.app.Dialog;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import java.util.Timer;
import java.util.TimerTask;

public class BattleResultDialogActivity extends FragmentActivity {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (RequestListener.Companion.getHasKIA()) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            BattleResultDialogFragment dialogFragment = new BattleResultDialogFragment(this::finish);
            dialogFragment.show(fragmentManager, "dialog");
        } else {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    RequestListener.Companion.setSendBattleResult(true);
                    RequestListener.Companion.getLatch().countDown();
                    finish();
                }
            }, 2000);
            Toast.makeText(this, R.string.press_back_to_proceed, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (!RequestListener.Companion.getHasKIA()) {
            RequestListener.Companion.setSendBattleResult(true);
            RequestListener.Companion.getLatch().countDown();
        }
        super.onBackPressed();
    }
}
