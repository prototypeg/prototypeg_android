package com.gitlab.prototypeg.android

import android.content.Context
import android.util.Log
import androidx.preference.PreferenceManager

import com.github.megatronking.netbare.http.HttpBody
import com.github.megatronking.netbare.http.HttpRequest
import com.github.megatronking.netbare.http.HttpResponse
import com.github.megatronking.netbare.http.HttpResponseHeaderPart
import com.github.megatronking.netbare.injector.InjectorCallback
import com.github.megatronking.netbare.injector.SimpleHttpInjector
import com.github.megatronking.netbare.stream.BufferStream
import com.gitlab.prototypeg.Session
import com.gitlab.prototypeg.network.request.DataRequest
import com.gitlab.prototypeg.network.request.Request
import com.gitlab.prototypeg.network.request.RequestFactory
import com.gitlab.prototypeg.network.response.ResponseFactory
import com.google.firebase.crashlytics.FirebaseCrashlytics

import org.apache.commons.httpclient.ChunkedInputStream
import org.apache.commons.httpclient.ChunkedOutputStream
import org.apache.commons.io.IOUtils
import org.threeten.bp.Instant
import java.io.*
import java.lang.Integer.min
import java.net.HttpURLConnection
import java.net.URL
import java.nio.ByteBuffer
import java.nio.charset.Charset
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream

class PacketInjector(private val session: Session, private val context: Context) : SimpleHttpInjector() {
    private var buffer: ByteArrayOutputStream? = null
    private var header: HttpResponseHeaderPart? = null

    private var request: Request? = null

    override fun sniffResponse(response: HttpResponse): Boolean {
        if (response.isHttps) return false
		if (response.host().canonicalHostName.equals("klanet.duckdns.org")) return false
		if (response.host().canonicalHostName.equals("gfkrcdn.17996cdn.net")) return false
		if (response.host().canonicalHostName.startsWith("sn-list")) {
			return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.high_quality_illustrations), false) && URL(response.url()).toURI().path.equals("/aNy0jv627jejqDIKgdldAlyQjnr7OExKF5k1daMC80I.txt")
		}
        return true
    }

    override fun sniffRequest(request: HttpRequest): Boolean {
        if (request.isHttps) return false
		if (request.host().canonicalHostName.equals("klanet.duckdns.org")) return false
		if (request.host().canonicalHostName.equals("gfkrcdn.17996cdn.net")) return false
        return true
    }

    @Throws(IOException::class)
    override fun onRequestInject(request: HttpRequest, body: HttpBody, callback: InjectorCallback) {
        try {
            val uri = request.path();
            if (uri!!.startsWith(session.uriHeader)) {
                this.request = RequestFactory[uri.substring(min(uri.length, session.uriHeader.length)), body.toBuffer().array()]
                session.networkManager.requestHandlerManager.handle(this.request!!)
                val buffer = ByteBuffer.allocate(this.request!!.buffer!!.size);
                buffer.put(this.request!!.buffer!!, 0, this.request!!.buffer!!.size)

                if (this.request!!.isEdited) {
                    callback.onFinished {
                        buffer
                    }
                } else {
                    callback.onFinished(body)
                }
            } else {
                callback.onFinished(body)
            }


        } catch (e: java.lang.Exception) {
            FirebaseCrashlytics.getInstance().recordException(e)
        }

    }

    @Throws(IOException::class)
    override fun onResponseInject(header: HttpResponseHeaderPart, callback: InjectorCallback) {
        Log.d("HEADERS", header.headers().toString())
        this.header = header
        Log.d("URL", header.uri().toString())
        callback.onFinished(header)
        buffer = ByteArrayOutputStream()
    }

    @Throws(IOException::class)
    override fun onResponseInject(httpResponse: HttpResponse, body: HttpBody, callback: InjectorCallback) {
        if ("chunked" != header!!.header("Transfer-Encoding") || "gzip" != header!!.header("Content-Encoding")) {
            Log.d("INDEX", String(body.toBuffer().array()))
            callback.onFinished(body)
            return
        }

        if (header!!.uri().path == "/index.php") {
            //server has something wrong
            callback.onFinished(body)
            return
        }

        val bytes = body.toBuffer().array()
        buffer!!.write(bytes)
        if (String(bytes).endsWith("\r\n\r\n")) {
            try {
                val inputStream = GZIPInputStream(
                        ChunkedInputStream(
                                ByteArrayInputStream(buffer!!.toByteArray())
                        )
                )
                val line = IOUtils.toByteArray(inputStream)
                inputStream.close()
                val uri = header!!.uri().path
                val newResponse: ByteArray
                if (uri!!.startsWith(session.uriHeader)) {
                    val response = ResponseFactory[
                            uri.substring(min(uri.length, session.uriHeader.length)),
                            line,
                            request!!
                    ]

                    try {
                        session.networkManager.responseHandlerManager.handle(response)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    if (response.isEdited) {
                        val outputStream = ByteArrayOutputStream()
                        val chunkedOutputStream = ChunkedOutputStream(outputStream)
                        val gzipOutputStream = GZIPOutputStream(chunkedOutputStream)
                        gzipOutputStream.write(response.buffer!!)
                        gzipOutputStream.finish()
                        chunkedOutputStream.finish()
                        newResponse = outputStream.toByteArray()
                        gzipOutputStream.close()
                    } else {
                        newResponse = buffer!!.toByteArray()
                    }
                } else if(httpResponse.host().hostName.startsWith("sn-list") && header!!.uri().path.equals("/aNy0jv627jejqDIKgdldAlyQjnr7OExKF5k1daMC80I.txt")) {
					try {
						val outputStream = ByteArrayOutputStream()
						val chunkedOutputStream = ChunkedOutputStream(outputStream)
						val gzipOutputStream = GZIPOutputStream(chunkedOutputStream)
						val file = FileInputStream(context.dataDir.absolutePath + "/update.bin")
						gzipOutputStream.write(file.readBytes())
						gzipOutputStream.finish()
						chunkedOutputStream.finish()
						newResponse = outputStream.toByteArray()
						gzipOutputStream.close()
					} catch (e: Throwable) {
						e.printStackTrace()
						return
					}

				} else {
                    newResponse = buffer!!.toByteArray()
                }
                buffer!!.close()
				(context.applicationContext as PrototypeG).lastResponseTime = Instant.now().plusSeconds(60 * 5)
                callback.onFinished(BufferStream(ByteBuffer.wrap(newResponse)))
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }
}
