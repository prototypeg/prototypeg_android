package com.gitlab.prototypeg.android;

import android.accessibilityservice.AccessibilityService;
import android.content.Intent;
import android.view.accessibility.AccessibilityEvent;

import androidx.preference.PreferenceManager;

import com.github.megatronking.netbare.NetBare;

import java.util.concurrent.TimeUnit;

public class DetectGFService extends AccessibilityService {

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
		boolean isRunning = false;
		boolean stop = true;
		if (event.getPackageName() == null) return;
		String packageName = event.getPackageName().toString();
		if (isGF(packageName)) {
			isRunning = true;
			stop = false;
		} else {
			if (packageName.startsWith("com.android")) {
				stop = false;
			}
		}
		if(isRunning) {
			Intent intent = NetBare.get().prepare();
			if (!NetBare.get().isActive()) {
				if (intent == null) {
					NetBare.get().start(((PrototypeG)this.getApplication()).getConfig());
				}
			}

		} else if (!((PrototypeG)getApplication()).getSwitch() && stop){
			NetBare.get().stop();
		}
    }

    public boolean isGF(String packageName) {
    	if (packageName.equals(getApplicationContext().getPackageName())) {
    		try {
				return !RequestListener.Companion.getLatch().await(0, TimeUnit.SECONDS);
			} catch (Exception e) {
    			return false;
			}
		}
        String packageNameShould = PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.package_name), "*");
        if (packageNameShould.equals("*")) {
			for (String s : getResources().getStringArray(R.array.client_package_values)) {
				if (s.equals(packageName)) return true;
			}
			return false;
		} else {
        	return packageNameShould.equals(packageName);
		}
    }

    @Override
    public void onInterrupt() {
		NetBare.get().stop();
    }
}

