package com.gitlab.prototypeg.android;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class BattleResultDialogFragment extends DialogFragment {
    final private Runnable finish;
    public BattleResultDialogFragment(Runnable finish) {
        this.finish = finish;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.settings_kia_protection));
        builder.setMessage(R.string.battle_result_send_dialog_message);
        builder.setPositiveButton(android.R.string.yes, (dialog, id) -> {
            RequestListener.Companion.setSendBattleResult(true);
            RequestListener.Companion.getLatch().countDown();
            finish.run();
        });
        builder.setNegativeButton(android.R.string.no, (dialog, id) -> {
            RequestListener.Companion.setSendBattleResult(false);
            RequestListener.Companion.getLatch().countDown();
            finish.run();
        });
        Dialog dialog = builder.create();
        dialog.getWindow().setType(
                Build.VERSION.SDK_INT < Build.VERSION_CODES.O
                        ? WindowManager.LayoutParams.TYPE_SYSTEM_DIALOG
                        : WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        );
        return dialog;
    }
}
