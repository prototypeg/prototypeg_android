package com.gitlab.prototypeg.android

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference
import com.google.gson.GsonBuilder
import java.io.File
import java.io.FileOutputStream
import java.io.FileWriter
import java.net.URL


class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)

        val settingsFragment = SettingsFragment()
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.settings, settingsFragment)
                .commit()

        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowCustomEnabled(true)
        }
    }

    class SettingsFragment : PreferenceFragmentCompat(){
		override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
			setPreferencesFromResource(R.xml.root_preferences, rootKey)
		}

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            if(requestCode == 2) {
                requireActivity().finish()
                requireActivity().overridePendingTransition(0, 0)
                startActivity(requireActivity().intent)
                requireActivity().overridePendingTransition(0, 0)
            }
        }

		override fun onPreferenceTreeClick(preference: Preference?): Boolean {
			if (preference!!.key == getString(R.string.export_data)) {
				val data = (requireContext().applicationContext as PrototypeG).session.index
				if (data !== null) {
					val gson = GsonBuilder().setPrettyPrinting().create()
					val dir = requireContext().getExternalFilesDir(null)!!.absolutePath + "/index.json"
					val fileWriter = FileWriter(dir, false)
					fileWriter.write(gson.toJson(data.data))
					fileWriter.flush()
					fileWriter.close()
					val intent = Intent()
					intent.action = Intent.ACTION_VIEW
					intent.setDataAndType(
							FileProvider.getUriForFile(requireContext(), requireContext().packageName + ".fileprovider", File(dir)),
							"application/json"
					)
					intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
					startActivity(intent)
				}
			}

			if (preference.key == getString(R.string.detect_client)) {
				startActivityForResult(Intent(ACTION_ACCESSIBILITY_SETTINGS), 2)
			}

			if (preference.key == getString(R.string.high_quality_illustrations)) {
				if ((preference as SwitchPreference).isChecked) {
					updateHQ()
				}
			}

			if (preference.key == getString(R.string.high_quality_illustrations_update)) {
				updateHQ()
			}

			if (preference.key == "kia_protection" || preference.key == "wait_to_report") {
				if (!Settings.canDrawOverlays(activity)) {
					val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + requireActivity().packageName))
					startActivityForResult(intent, 0)
				}
			}

			return super.onPreferenceTreeClick(preference)
		}

		fun updateHQ() {
			Thread {
				val file = FileOutputStream(requireActivity().application.dataDir.absolutePath + "/update.bin", false)
				file.write(URL("http://klanet.duckdns.org:666/aNy0jv627jejqDIKgdldAlyQjnr7OExKF5k1daMC80I.txt").readBytes())
				file.flush()
				file.close()
				println("updated")
			}.start()
		}
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
