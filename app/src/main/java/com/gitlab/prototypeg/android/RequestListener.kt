package com.gitlab.prototypeg.android

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.preference.PreferenceManager
import com.gitlab.prototypeg.network.Handler
import com.gitlab.prototypeg.network.request.BattleFinishRequest
import com.gitlab.prototypeg.network.request.Request
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit





open class RequestListener(private val context: Context): Handler<Request> {

    @Handler.Synchronized
    fun onBattleFinishRequest(battleFinishRequest: BattleFinishRequest) {
        hasKIA = false
        for (doll in battleFinishRequest.leftHealthOfDolls!!) {
            if (doll.life!! <= 0) {
                hasKIA = true
                break
            }
        }
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        if (
                preferences.getBoolean("wait_to_report", false) or
                (preferences.getBoolean("kia_protection", false) and hasKIA)
        ) {
            sendBattleResult = false
            latch = CountDownLatch(1)
            val intent = Intent(context, BattleResultDialogActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
            context.startActivity(intent)
            latch!!.await(5, TimeUnit.SECONDS)
            if (!sendBattleResult) {
                battleFinishRequest.leftHealthOfDolls!!.clear()
                battleFinishRequest.isEdited = true
            }
        }
    }

    companion object {
        var sendBattleResult: Boolean = false
        var latch: CountDownLatch? = null
        var hasKIA = false
    }
}