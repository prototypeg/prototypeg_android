package com.gitlab.prototypeg.android

import android.app.Notification
import android.app.PendingIntent
import android.content.Intent
import androidx.core.app.NotificationCompat
import com.github.megatronking.netbare.NetBare

import com.github.megatronking.netbare.NetBareService
import org.threeten.bp.Instant
import java.util.*
import java.util.concurrent.atomic.AtomicInteger

class VpnService : NetBareService() {
    override fun notificationId(): Int {
        return (applicationContext as PrototypeG).newNotificationId()
    }

    override fun createNotification(): Notification {

		val intent = Intent(this, MainActivity::class.java)
		val pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val builder = NotificationCompat.Builder(this, PrototypeG.VPN_SERVICE)
                .setSmallIcon(android.R.drawable.ic_lock_idle_lock)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
				.addAction(android.R.drawable.ic_menu_more, getString(R.string.open), pendingIntent)
        return builder.build()
    }
	/*
	override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
		val timer = Timer()
		val prototypeG = applicationContext as PrototypeG
		timer.schedule(object: TimerTask() {
			override fun run() {
				if (prototypeG.lastResponseTime.isBefore(Instant.now())) {
					NetBare.get().stop()
				} else {
					timer.schedule(this, prototypeG.lastResponseTime.toEpochMilli() - Instant.now().toEpochMilli() + 5)
				}
			}
		}, 5 * 60 * 1000)
		return super.onStartCommand(intent, flags, startId)
	}*/

    override fun onDestroy() {
        super.onDestroy()
    }
}
