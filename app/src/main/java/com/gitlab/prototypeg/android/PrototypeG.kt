package com.gitlab.prototypeg.android

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.SharedPreferences
import android.os.Build

import androidx.preference.PreferenceManager

import com.github.megatronking.netbare.NetBare
import com.github.megatronking.netbare.NetBareConfig
import com.github.megatronking.netbare.http.HttpInjectInterceptor
import com.github.megatronking.netbare.http.HttpInterceptorFactory
import com.github.megatronking.netbare.ssl.JKS
import com.gitlab.prototypeg.Session
import org.threeten.bp.Instant
import java.net.URL
import java.util.Arrays
import java.util.concurrent.atomic.AtomicInteger

class PrototypeG : Application() {
    val session = Session()

    var jks: JKS? = null
        private set


    val config: NetBareConfig
        get() {
			val configBuilder = NetBareConfig.defaultHttpConfig(jks!!, listOf(
					HttpInjectInterceptor.createFactory(
							PacketInjector(session, this)
					)
			)).newBuilder()
			val preferences = PreferenceManager.getDefaultSharedPreferences(this)
			val packageName = preferences.getString(getString(R.string.package_name), "none")
			if (packageName == "*") {
				for (s in resources.getStringArray(R.array.client_package_values)) {
					if (s == "*") {
						continue
					}
					configBuilder.addAllowedApplication(s)
				}
			} else {
				configBuilder.addAllowedApplication(packageName!!)
			}
			configBuilder.excludeSelf(true)
			return configBuilder.build()
		}

	private val atomicInteger = AtomicInteger(0)

	var lastResponseTime: Instant = Instant.now().plusSeconds(60 * 5)

	var switch = false

    override fun onCreate() {
        super.onCreate()

        val preferences = PreferenceManager.getDefaultSharedPreferences(this)

        object : Thread() {
            override fun run() {
                Session.init()
            }
        }.start()

        jks = JKS(
                this, this.getString(R.string.app_name),
                this.getString(R.string.app_name).toCharArray(),
                this.getString(R.string.app_name),
                this.getString(R.string.app_name),
                this.getString(R.string.app_name),
                this.getString(R.string.app_name),
                this.getString(R.string.app_name)
		)

		/*
        if (!JKS.isInstalled(this, getString(R.string.app_name))) {
            try {
                JKS.install(this, this.getString(R.string.app_name), this.getString(R.string.app_name))
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        */

        registerNotificationChannels()

        NetBare.get().attachApplication(this, false)

        session.eventManager.registerListener(EventListener(this))
		session.networkManager.requestHandlerManager.registerHandler(RequestListener(this))
		session.networkManager.responseHandlerManager.registerHandler(ResponseListener(this))
    }

    private fun registerNotificationChannels() {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			var notificationChannels : List<NotificationChannel> = listOf(
					NotificationChannel(
							VPN_SERVICE,
							getString(R.string.channel_vpn_service),
							NotificationManager.IMPORTANCE_MIN
					),
					NotificationChannel(
							BUILD,
							getString(R.string.channel_build),
							NotificationManager.IMPORTANCE_HIGH
					),
					NotificationChannel(
							NOTICE,
							getString(R.string.channel_notice),
							NotificationManager.IMPORTANCE_HIGH
					),
					NotificationChannel(
							GET,
							getString(R.string.channel_get),
							NotificationManager.IMPORTANCE_HIGH
					),
					NotificationChannel(
							BATTLE_FINISH,
							getString(R.string.channel_battle_finish),
							NotificationManager.IMPORTANCE_HIGH
					)
			)
			notificationChannels = notificationChannels.filter { notificationManager.getNotificationChannel(it.id) === null }
			notificationManager.createNotificationChannels(notificationChannels)
		}
    }

	fun newNotificationId(): Int {
		return atomicInteger.incrementAndGet()
	}

	companion object {
		const val VPN_SERVICE = "vpn_service"
		const val BUILD = "build"
		const val NOTICE = "notice"
		const val GET = "get"
		const val BATTLE_FINISH = "battle_finish"
	}
}
