package com.gitlab.prototypeg.android

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Switch

import com.github.megatronking.netbare.NetBare


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
		val netbare = NetBare.get().prepare()
		if (netbare != null) {
			startActivityForResult(netbare, 3)
		}

        (findViewById<View>(R.id.vpn_service) as Switch).setOnCheckedChangeListener { _, isChecked ->
			(application as PrototypeG).switch = isChecked
            if (isChecked) {
				val netbare = NetBare.get().prepare()
                if (netbare == null) {
					println(NetBare.get().isActive)
					if (!NetBare.get().isActive) {
						NetBare.get().start((application as PrototypeG).config)
					}

					startGF()
                }
            } else {
				NetBare.get().stop()
				println(NetBare.get().isActive)
			}
        }

        findViewById<View>(R.id.start_gf).setOnClickListener { startGF() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
			val netbare = NetBare.get().prepare()
			if (netbare != null) {
				startActivityForResult(netbare, 3)
			}
		}
    }

    private fun startGF() {
        val launchIntent = packageManager.getLaunchIntentForPackage(
                PreferenceManager.getDefaultSharedPreferences(this)
                        .getString(getString(R.string.package_name), "")!!
        )
        if (launchIntent != null) {
            startActivity(launchIntent)
        }
		//possibly there no package with; not handling
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.preference) {
            startActivity(Intent(this, SettingsActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }
}
